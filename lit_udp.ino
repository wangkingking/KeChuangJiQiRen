#include <SPI.h>        
#include <Ethernet.h>
#include <EthernetUdp.h>        
/********************UDP网络部分**********************/
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
IPAddress ip(169, 254, 225, 101);         //IP地址
unsigned int localPort = 8889;             //监听端口
char packetBuffer[UDP_TX_PACKET_MAX_SIZE]; //接收缓冲区
char received_data[] = "received_data";    //接收数据成功后向服务器发送的字符
char finished_action[] = "finished_action";//动作完成后向服务器发送的字符
int packetSize=0;                          //数据大小
EthernetUDP Udp;                           //定义一个UDP对象
/******************************************************/
/********************数据解析部分**********************/
unsigned int using_data_z[6] = {0};
unsigned int using_data[6] = {0,680};                      //拆分后的数字数组
/******************************************************/
/********************动作执行部分**********************/
unsigned int command_data = 0;
unsigned int data1 = 0;
unsigned int data2 = 0;

unsigned int run_speed = 150;
unsigned int reset_speed = 2000;
int en = 0;
unsigned int run_step = 0;
unsigned int now_location = 0;
/******************************************************/


void setup() 
{
  pinMode(2,INPUT);  //限位
  pinMode(8,OUTPUT); //脉冲
  pinMode(9,OUTPUT); //方向
  pinMode(11,OUTPUT);//使能

  digitalWrite(8,HIGH);
  digitalWrite(9,HIGH);
  digitalWrite(11,LOW);
  //初始化网络和UDP链接
  Ethernet.begin(mac, ip);
  Udp.begin(localPort);
  //开始串口
  Serial.begin(9600);
  reset_location();
}

void loop() 
{
  packetSize = Udp.parsePacket(); //读取数据缓冲区信息长度到packetSize
  if (packetSize)                     //如果数据缓冲区有数据
  {
    print_packetsize();
    print_ip();
    print_port();
    receivedata();                    //读数据到数据缓冲区packetBuffer
    sendtoserver(received_data);      //收到数据，向服务器发送接收应答信号
    /***********执行动作************/
    data_parsing();                   //解析服务器数据存储到usiing_data数组
    start_run(data1);
    //clean_data();
    /*******************************/
    sendtoserver(finished_action);    //动作执行完毕，向服务器发送动作完成信号
    Serial.println();
  }
}//此处loop()主函数结束
/***************动作执行函数*****************/
void start_run(int m)
{
    if(m>now_location)
    {
          digitalWrite(9,HIGH);
          run_step = m-now_location;
          now_location = m;
          for(int i=0; i<run_step;i++)
          {
                digitalWrite(8,HIGH);
                delayMicroseconds(150);
                digitalWrite(8,LOW);
                delayMicroseconds(150); 
          }
    }
    if(m<now_location)
    {
          digitalWrite(9,LOW);
          run_step = now_location-m;
          now_location = m;
          for(int i=0; i<run_step;i++)
          {
                digitalWrite(8,HIGH);
                delayMicroseconds(run_speed);
                digitalWrite(8,LOW);
                delayMicroseconds(run_speed); 
          }
    }  
    clean_data();//清除数据，不然出错
}
void reset_location()
{ 
    digitalWrite(9,LOW);
    while(digitalRead(2) == 0)  
    {
        digitalWrite(8,HIGH);
        delayMicroseconds(reset_speed);
        digitalWrite(8,LOW);
        delayMicroseconds(reset_speed);
    }  
    now_location = 0;
}

/***************数据解析函数*****************/
void clean_data()
{Serial.println(data1);
    command_data = 0;
    data1 = 0;
    data2 = 0;
    Serial.println(data1);
}
void data_parsing()
{
    int j = 0;
    for(int i = 0; i < String(packetBuffer).length() ; i++)
    {
        if(packetBuffer[i] == ',')
        {
            j++;
        }
        else
        {
            using_data_z[j] = using_data_z[j] * 10 + (packetBuffer[i] - '0');
        }
    }
    command_data = using_data_z[0];
    data1 = using_data_z[1];
    data2 = using_data_z[2];
    for(int i = 0; i <j+1; i++)
    {
      using_data_z[i] = 0;
    }
}
/***************UDP网络函数******************/
void receivedata()
{//UDP_TX_PACKET_MAX_SIZE
    Udp.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);
    Serial.print("The information is :");
    Serial.println(packetBuffer);  
}
void sendtoserver(char send_data[])
{
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(send_data);
    Udp.endPacket();      
}
void print_packetsize()
{
    Serial.print("The packet size is: ");
    Serial.println(packetSize);  
}
void print_port()
{
    Serial.print("The port is :");
    Serial.println(Udp.remotePort());  
}
void print_ip()
{
    IPAddress remote = Udp.remoteIP();
    Serial.print("The information from :");
    for (int i = 0; i < 4; i++)
    {
      Serial.print(remote[i], DEC);
      if (i < 3)
      {
        Serial.print(".");
      }
    }
    Serial.println();  
}

