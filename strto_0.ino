String comdata = "10101,20202";
//numdata是分拆之后的数字数组
int numdata[6] = {0}, mark = 0;

void setup()
{
  Serial.begin(9600);
  mark = 1;
}

void loop()
{
  if(mark == 1)  //如果接收到数据则执行comdata分析操作，否则什么都不做。
  {
    //j是分拆之后数字数组的位置记数
    int j = 0;
    /*******************下面是重点*******************/
    for(int i = 0; i < comdata.length() ; i++)
    {
      if(comdata[i] == ',')
      {
        j++;
      }
      else
      {
        numdata[j] = numdata[j] * 10 + (comdata[i] - '0');
      }
    }
    for(int i = 0; i <j+1; i++)
    {
      Serial.print("data ");
      Serial.print(i);
      Serial.print(" = ");
      Serial.println(numdata[i]);
      numdata[i] = 0;
    }
    comdata = String("");//清空comdata
    mark = 0;//输出之后必须将读到数据的mark置0，不置0下次循环就不能使用了。
  }
}

